
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ClientClass {

	BufferedReader in;
	PrintWriter out;
	JFrame frame = new JFrame("Chatter");
	JTextField textField = new JTextField(40);
	JTextArea messageArea = new JTextArea(8, 40);

	JSONObject json = new JSONObject();
	JSONParser parser = new JSONParser();
	String line;
	String answer = "";
	Object object;

	String username;
	String encryptionType;

	long p, g, sa, sb, value;
	Integer generateC;

	public ClientClass() {

		textField.setEditable(false);
		messageArea.setEditable(false);
		frame.getContentPane().add(textField, "North");
		frame.getContentPane().add(new JScrollPane(messageArea), "Center");
		frame.pack();

		textField.addActionListener(new ActionListener() {

			@SuppressWarnings("unchecked")
			public void actionPerformed(ActionEvent e) {
				// We are sending encrypting and message here

				if (!textField.getText().equals("")) {
					json.clear();
					json.put("msg", getEncyptedMessageForSend(textField.getText(), encryptionType, value));
					json.put("from", username);
					out.println(json);
					textField.setText("");
				}
			}
		});
	}

	private String getServerAddress() {
		return JOptionPane.showInputDialog(frame, "Enter IP Address of the Server:", "Welcome to the Chatter",
				JOptionPane.QUESTION_MESSAGE);
	}

	private String getName() {
		return JOptionPane.showInputDialog(frame, "Choose a screen name:", "Screen name selection",
				JOptionPane.PLAIN_MESSAGE);
	}

	private String getEncryption() {
		return JOptionPane.showInputDialog(frame, "Choose a encryption:", "Encryption selection [none/xor/cezar]",
				JOptionPane.PLAIN_MESSAGE);
	}

	private void run() throws IOException {
		p = 0;
		String serverAddress = getServerAddress();
		System.out.println(serverAddress);
		Socket socket = new Socket(serverAddress, 9001);
		in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		out = new PrintWriter(socket.getOutputStream(), true);

		while (true) {
			line = in.readLine();
			System.out.println(line);

			try {
				object = parser.parse(line);
				json = (JSONObject) object;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			answer = (String) json.get("command");

			try {
				if (answer.equals("submitname")) {
					username = getName();
					out.println(username);
					line = in.readLine();
					try {
						object = parser.parse(line);
						json = (JSONObject) object;
					} catch (ParseException e) {
						e.printStackTrace();
					}
					answer = (String) json.get("command");
					if (answer.equals("nameaccepted")) {
						textField.setEditable(true);
						do {
							encryptionType = getEncryption();
						} while (!encryptionType.equals("none") && !encryptionType.equals("xor")
								&& !encryptionType.equals("cezar"));
						json.clear();
						json.put("encryption", encryptionType);
						out.println(json);
					}
				}
			} catch (NullPointerException e) {
				System.out.println("MSG SENT");
			}

			// DH protocol
			long dh = diffieHellman();
			System.out.println(dh);

			// Getting messages and decrypting them
			while (true) {
				answer = in.readLine();
				try {
					object = parser.parse(answer);
					json = (JSONObject) object;
					String msg = (String) json.get("msg");
					username = (String) json.get("from");
					if(!object.equals(null)){
						messageArea.append(username + ": " + getDencyptedMessageToRead(msg, encryptionType, value) + "\n");
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) throws Exception {
		ClientClass client = new ClientClass();
		client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		client.frame.setVisible(true);
		client.run();
	}

	@SuppressWarnings("unchecked")
	public long diffieHellman() {
		try {
			json.clear();
			json.put("request", "keys");
			System.out.println(json);
			out.println(json);

			line = in.readLine();
			System.out.println("p i g" + line);
			object = parser.parse(line);
			json = (JSONObject) object;
			p = (long) json.get("p");
			System.out.println("p" + p);
			if (!(p == 0)) {
				g = (long) json.get("g");
				System.out.println("g" + g);
				generateC = generate();
				System.out.println("wygenerowane" + generateC);
				sa = calc(p, g, generateC);
				System.out.println("sa" + sa);
				json.clear();
				json.put("a", sa);
				out.println(json);
				answer = in.readLine();
				System.out.println("b" + answer);
				object = parser.parse(answer);
				json = (JSONObject) object;
				sb = (long) json.get("b");
				System.out.println("sb" + sb);
				System.out.println("wygenerowane" + generateC);
				value = calcValue(sb, generateC, p);
				System.out.println("value" + value);
			}
		} catch (IOException e) {
		} catch (org.json.simple.parser.ParseException e) {
		}
		return value;
	}

	// Generate random value
	public int generate() {
		Random random = new Random();
		int value = random.nextInt(10);
		return value;
	}

	// Calculate (g^generate) mod p
	public long calc(long p, long g, int generate) {
		long value = (long) (Math.pow(g, generate) % p);
		return value;
	}

	// Calculate (sb^generate) mod p
	public long calcValue(long sb, int generate, long p) {
		long value = (long) (Math.pow(sb, generate) % p);
		return value;
	}

	// Return encrypted message for sending
	private String getEncyptedMessageForSend(String msg, String encryptionType, long key) {
		if (encryptionType.equals("none")) {
			return encodeBase64(msg);
		}
		if (encryptionType.equals("xor")) {
			String xor = encodeBase64(codeDecodeXor(msg, key));
			return xor;
		}
		if (encryptionType.equals("cezar")) {
			String cezar = encodeBase64(codeCezar(msg, key));
			return cezar;
		} else {
			return null;
		}
	}

	// Return decrypted message
	private String getDencyptedMessageToRead(String msg, String encryptionType, long key) {
		if (encryptionType.equals("none")) {
			return decodeBase64(msg);
		}
		if (encryptionType.equals("xor")) {
			String xor = codeDecodeXor(decodeBase64(msg), key);
			return xor;
		}
		if (encryptionType.equals("cezar")) {
			String cezar = decodeCezar(decodeBase64(msg), key);
			return cezar;
		} else {
			return null;
		}
	}

	// Code to Base64
	private String encodeBase64(String string) {
		byte[] bytes = Base64.encodeBase64(string.getBytes());
		return new String(bytes);
	}

	// Decode from Base64
	private String decodeBase64(String string) {
		byte[] bytes = Base64.decodeBase64(string.getBytes());
		return new String(bytes);
	}

	// Code to/decode from XOR algorithm
	private String codeDecodeXor(String message, long key) {
		String xor = "";
		for (int i = 0; i < message.length(); i++) {
			xor += (char) (message.charAt(i) ^ key);
		}
		return xor;
	}

	// Code to Cezar
	private String codeCezar(String message, long key) {
		String cezar = "";
		for (int i = 0; i < message.length(); i++) {
			cezar += (char) (message.charAt(i) + key);
		}
		return cezar;
	}

	// Decode from Cezar
	private String decodeCezar(String message, long key) {
		String cezar = "";
		for (int i = 0; i < message.length(); i++) {
			cezar += (char) (message.charAt(i) - key);
		}
		return cezar;
	}

}