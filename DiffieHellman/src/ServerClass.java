
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.ParseException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class ServerClass {

	private static final int PORT = 9001;

	private static HashSet<String> names = new HashSet<String>();

	private static HashMap<PrintWriter, String[]> writers = new HashMap<>();

	public static void main(String[] args) throws Exception {
		System.out.println("The chat server is running.");
		ServerSocket listener = new ServerSocket(PORT);
		try {
			while (true) {
				new Handler(listener.accept()).start();
			}
		} finally {
			listener.close();
		}
	}

	private static class Handler extends Thread {
		String name;
		Socket socket;
		BufferedReader in;
		PrintWriter out;

		JSONObject json = new JSONObject();
		JSONParser parser = new JSONParser();
		String line;
		String answer = "";
		Object object;

		long p, g, sa, sb, value;
		Integer generateS;

		public Handler(Socket socket) {
			this.socket = socket;
		}

		@SuppressWarnings("unchecked")
		public void run() {
			String encryptionType = "none";

			try {

				in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);

				while (true) {
					json.clear();
					json.put("command", "submitname");
					System.out.println(json.toJSONString());
					out.println(json);

					name = in.readLine();
					if (name == null) {
						return;
					}
					synchronized (names) {
						if (!names.contains(name)) {
							names.add(name);
							break;
						}
					}
				}

				json.clear();
				json.put("command", "nameaccepted");
				System.out.println(json.toJSONString());
				out.println(json);

				// Get type of encryption
				answer = in.readLine();
				try {
					object = parser.parse(answer);
					json = (JSONObject) object;
					encryptionType = (String) json.get("encryption");
					System.out.println(encryptionType);
				} catch (org.json.simple.parser.ParseException e) {
				}

				// DH protocol
				long dh = diffieHellman();
				System.out.println(dh);
				
				String[] clients = new String[2];
				clients[0] = "" + value;
				clients[1] = encryptionType;
				writers.put(out, clients);

				// Getting messages and decrypting them
				while (true) {
					String message = null;
					answer = in.readLine();
					try {
						object = parser.parse(answer);
						json = (JSONObject) object;
						String msg = (String) json.get("msg");
						name = (String) json.get("from");
						if(!object.equals(null)){
							message = getDencyptedMessageToRead(msg, encryptionType, value);
						}
					} catch (Exception e) {
					}
					for (PrintWriter writer : writers.keySet()) {
						String codedMessage = getEncyptedMessageForSend(message, encryptionType, value, writer);
						json.clear();
						json.put("from", name);
						json.put("msg", codedMessage);
						writer.println(json);
				}
				}
				
			} catch (IOException e) {
				System.out.println(e);
			} finally {
				if (name != null) {
					names.remove(name);
				}
				if (out != null) {
					writers.remove(out);
				}
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}

		public long diffieHellman() {
			try {
				line = in.readLine();
				System.out.println(line);
				object = parser.parse(line);
				json = (JSONObject) object;
				answer = (String) json.get("request");
				System.out.println(answer);
				if (answer.equals("keys")) {
					json.clear();
					p = 17;
					g = 31;
					json.put("p", p);
					json.put("g", g);
					out.println(json);
					System.out.println(json);
					generateS = generate();
					System.out.println("wygenerowany" + generateS);
					sb = calc(p, g, generateS);
					System.out.println("sb" + sb);
					json.clear();
					json.put("b", sb);
					out.println(json);
					answer = in.readLine();
					System.out.println("sa" + answer);
					object = parser.parse(answer);
					json = (JSONObject) object;
					sa = (long) json.get("a");
					System.out.println("sa " + sa);
					System.out.println("wygenerowany" + generateS);
					value = calcValue(sa, generateS, p);
					System.out.println("value" + value);
				}
			} catch (IOException e) {
			} catch (org.json.simple.parser.ParseException e) {
			}
			return value;
		}

		public int generate() {
			Random random = new Random();
			int value = random.nextInt(10);
			return value;
		}

		public long calc(long p, long g, int generate) {
			long value = (long) (Math.pow(g, generate) % p);
			return value;
		}

		public long calcValue(long sa, int generate, long p) {
			long value = (long) (Math.pow(sa, generate) % p);
			return value;
		}

		// Return encrypted message for sending
		private String getEncyptedMessageForSend(String msg, String encryptionType, long key, PrintWriter writer) {
			if (writers.get(writer)[1].equals("none")) {
				return encodeBase64(msg);
			}
			if (writers.get(writer)[1].equals("xor")) {
				String xor = encodeBase64(codeDecodeXor(msg, key));
				return xor;
			}
			if (writers.get(writer)[1].equals("cezar")) {
				String cezar = encodeBase64(codeCezar(msg, key));
				return cezar;
			} else {
				return null;
			}
		}

		// Return decrypted message
		private String getDencyptedMessageToRead(String msg, String encryptionType, long key) {
			if (encryptionType.equals("none")) {
				return decodeBase64(msg);
			}
			if (encryptionType.equals("xor")) {
				String xor = codeDecodeXor(decodeBase64(msg), key);
				return xor;
			}
			if (encryptionType.equals("cezar")) {
				String cezar = decodeCezar(decodeBase64(msg), key);
				return cezar;
			} else {
				return null;
			}
		}

		// Code to Base64
		private String encodeBase64(String string) {
			byte[] bytes = Base64.encodeBase64(string.getBytes());
			return new String(bytes);
		}

		// Decode from Base64
		private String decodeBase64(String string) {
			byte[] bytes = Base64.decodeBase64(string.getBytes());
			return new String(bytes);
		}

		// Code to/decode from XOR algorithm
		private String codeDecodeXor(String message, long key) {
			String xor = "";
			for (int i = 0; i < message.length(); i++) {
				xor += (char) (message.charAt(i) ^ key);
			}
			return xor;
		}

		// Code to Cezar
		private String codeCezar(String message, long key) {
			String cezar = "";
			for (int i = 0; i < message.length(); i++) {
				cezar += (char) (message.charAt(i) + key);
			}
			return cezar;
		}

		// Decode from Cezar
		private String decodeCezar(String message, long key) {
			String cezar = "";
			for (int i = 0; i < message.length(); i++) {
				cezar += (char) (message.charAt(i) - key);
			}
			return cezar;
		}
	}
}