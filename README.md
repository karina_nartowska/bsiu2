# README #

### Przeznaczenie projektu ###

Zadanie na laboratorium z kursu Bezp. syst. i usług inform. 2. 

### Wymagania systemowe ###

* Docelowy system operacyjny to Windows 7 (projekt powinien też działać na systemach Linux)
* Eclipse Mars.2 Release (4.5.2)
* Java 1.8
* json-simple-1.1.1.jar

### Sposób uruchomienia projektu ###

Pierwsze zadanie laboratoryjne znajduje się na branchu lab01 - należy go zaimportować do Eclipse'a, dodać bibliotekę json-simple-1.1.1.jar i uruchomić ClienClass.java oraz ServerClass.java.